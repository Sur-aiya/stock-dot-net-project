USE [master]
GO
/****** Object:  Database [StockManagementDB_New]    Script Date: 3/25/2019 10:20:44 AM ******/
CREATE DATABASE [StockManagementDB_New]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'StockManagementDB_New', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\StockManagementDB_New.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'StockManagementDB_New_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\StockManagementDB_New_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [StockManagementDB_New] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [StockManagementDB_New].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [StockManagementDB_New] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET ARITHABORT OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [StockManagementDB_New] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [StockManagementDB_New] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [StockManagementDB_New] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET  DISABLE_BROKER 
GO
ALTER DATABASE [StockManagementDB_New] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [StockManagementDB_New] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [StockManagementDB_New] SET  MULTI_USER 
GO
ALTER DATABASE [StockManagementDB_New] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [StockManagementDB_New] SET DB_CHAINING OFF 
GO
ALTER DATABASE [StockManagementDB_New] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [StockManagementDB_New] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [StockManagementDB_New]
GO
/****** Object:  Table [dbo].[CompanySetup]    Script Date: 3/25/2019 10:20:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanySetup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CompanySetup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemSetup]    Script Date: 3/25/2019 10:20:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemSetup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NULL,
	[CompanyId] [int] NULL,
	[ItemName] [varchar](50) NULL,
	[ReorderLevel] [int] NULL,
 CONSTRAINT [PK_ItemSetup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 3/25/2019 10:20:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NULL,
 CONSTRAINT [PK_ProductCatagory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockIn]    Script Date: 3/25/2019 10:20:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockIn](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NULL,
	[ItemId] [int] NULL,
	[StockQuantity] [int] NULL,
 CONSTRAINT [PK_StockIn] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StockOutSell]    Script Date: 3/25/2019 10:20:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockOutSell](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [int] NULL,
	[Sell] [int] NULL,
	[Date] [smalldatetime] NULL,
 CONSTRAINT [PK_StockOutSubtract] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[CompanySetup] ON 

INSERT [dbo].[CompanySetup] ([Id], [CompanyName]) VALUES (1, N'Pran')
INSERT [dbo].[CompanySetup] ([Id], [CompanyName]) VALUES (2, N'Radhuni')
INSERT [dbo].[CompanySetup] ([Id], [CompanyName]) VALUES (3, N'Hwakings')
INSERT [dbo].[CompanySetup] ([Id], [CompanyName]) VALUES (4, N'Huwai')
INSERT [dbo].[CompanySetup] ([Id], [CompanyName]) VALUES (5, N'Walton')
SET IDENTITY_INSERT [dbo].[CompanySetup] OFF
SET IDENTITY_INSERT [dbo].[ItemSetup] ON 

INSERT [dbo].[ItemSetup] ([Id], [CategoryId], [CompanyId], [ItemName], [ReorderLevel]) VALUES (1, 1, 1, N'Mango Juice', 10)
INSERT [dbo].[ItemSetup] ([Id], [CategoryId], [CompanyId], [ItemName], [ReorderLevel]) VALUES (2, 2, 2, N'Red Chili', 10)
INSERT [dbo].[ItemSetup] ([Id], [CategoryId], [CompanyId], [ItemName], [ReorderLevel]) VALUES (3, 3, 3, N'Tawa', 0)
INSERT [dbo].[ItemSetup] ([Id], [CategoryId], [CompanyId], [ItemName], [ReorderLevel]) VALUES (4, 3, 3, N'Spoon', 0)
INSERT [dbo].[ItemSetup] ([Id], [CategoryId], [CompanyId], [ItemName], [ReorderLevel]) VALUES (5, 4, 5, N'Oven', 20)
SET IDENTITY_INSERT [dbo].[ItemSetup] OFF
SET IDENTITY_INSERT [dbo].[ProductCategory] ON 

INSERT [dbo].[ProductCategory] ([Id], [CategoryName]) VALUES (1, N'Juice')
INSERT [dbo].[ProductCategory] ([Id], [CategoryName]) VALUES (2, N'Spices')
INSERT [dbo].[ProductCategory] ([Id], [CategoryName]) VALUES (3, N'Kitchen Items')
INSERT [dbo].[ProductCategory] ([Id], [CategoryName]) VALUES (4, N'Electronics')
SET IDENTITY_INSERT [dbo].[ProductCategory] OFF
SET IDENTITY_INSERT [dbo].[StockIn] ON 

INSERT [dbo].[StockIn] ([Id], [CompanyId], [ItemId], [StockQuantity]) VALUES (1, 1, 1, 20)
INSERT [dbo].[StockIn] ([Id], [CompanyId], [ItemId], [StockQuantity]) VALUES (2, 2, 2, 60)
INSERT [dbo].[StockIn] ([Id], [CompanyId], [ItemId], [StockQuantity]) VALUES (3, 3, 3, 40)
INSERT [dbo].[StockIn] ([Id], [CompanyId], [ItemId], [StockQuantity]) VALUES (4, 5, 5, 20)
SET IDENTITY_INSERT [dbo].[StockIn] OFF
SET IDENTITY_INSERT [dbo].[StockOutSell] ON 

INSERT [dbo].[StockOutSell] ([Id], [ItemId], [Sell], [Date]) VALUES (1, 1, 30, CAST(0xAA1A04A7 AS SmallDateTime))
INSERT [dbo].[StockOutSell] ([Id], [ItemId], [Sell], [Date]) VALUES (2, 3, 10, CAST(0xAA1A0505 AS SmallDateTime))
INSERT [dbo].[StockOutSell] ([Id], [ItemId], [Sell], [Date]) VALUES (3, 5, 10, CAST(0xAA1A0513 AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[StockOutSell] OFF
ALTER TABLE [dbo].[ItemSetup]  WITH CHECK ADD  CONSTRAINT [FK_ItemSetup_CompanySetup] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[CompanySetup] ([Id])
GO
ALTER TABLE [dbo].[ItemSetup] CHECK CONSTRAINT [FK_ItemSetup_CompanySetup]
GO
ALTER TABLE [dbo].[ItemSetup]  WITH CHECK ADD  CONSTRAINT [FK_ItemSetup_ProductCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ProductCategory] ([Id])
GO
ALTER TABLE [dbo].[ItemSetup] CHECK CONSTRAINT [FK_ItemSetup_ProductCategory]
GO
ALTER TABLE [dbo].[StockOutSell]  WITH CHECK ADD  CONSTRAINT [FK_StockOutSell_ItemSetup] FOREIGN KEY([ItemId])
REFERENCES [dbo].[ItemSetup] ([Id])
GO
ALTER TABLE [dbo].[StockOutSell] CHECK CONSTRAINT [FK_StockOutSell_ItemSetup]
GO
USE [master]
GO
ALTER DATABASE [StockManagementDB_New] SET  READ_WRITE 
GO
